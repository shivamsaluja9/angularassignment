import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

@Component({
    selector: 'app-student',
    templateUrl: './student.component.html',
    styleUrls: ['./student.component.css']
})
/** student component*/
export class StudentComponent {
    /** student ctor */
  StudentData: any
  constructor(private hhtp: Http, private route: Router) {
      this.GetData()
  }
  GetData() {
    this.hhtp.get('https://localhost:44382/api/Student/GetStudentDetails').subscribe(response => {
      console.log(response.json());
      this.StudentData = response.json();
    });
  }

  getDetails(searchby, searchvalue) {
   
    this.hhtp.get('https://localhost:44382/api/Student/GetStudentDetailsByFilter?SearchBy='+searchby+'&SearchValue='+searchvalue).subscribe(response => {
      console.log(response.json());
      this.StudentData = response.json();
    });
  }

  DeleteStudent(id) {
    this.hhtp.get('https://localhost:44382/api/Student/DeleteStudent?id=' + id).subscribe(response => {
      alert('Deleted Successfully');
      this.GetData()
    });
  }

}
