﻿using AngularAssignment.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;

namespace AngularAssignment.DAL
{
    public class StudentContext : DbContext
    {


        public StudentContext(DbContextOptions options)
            : base(options)
        {
        }
        public DbSet<StudentMDL> Students { get; set; }
        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{

        //    modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        
        //}
       
    }
}