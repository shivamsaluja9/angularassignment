﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AngularAssignment.DAL;
using AngularAssignment.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AngularAssignment.Controllers
{
    [Route("api/{controller}/{action}")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly StudentContext _context;

        public StudentController(StudentContext context)
        {
            _context = context;
        }
        [HttpPost]
       // [Route("api/Student/AddStudentDetails")]
        public ActionResult<StudentMDL> AddStudentDetails(StudentMDL obj) {
            //using (var _context=new StudentContext())
            //{
                _context.Students.Add(obj);
                _context.SaveChanges();
            //}
              
            return CreatedAtAction(nameof(GetStudentDetails), new { id = obj.ID }, obj);
        }

        [HttpGet]
        //[Route("api/Student/GetTodoItem")]
        public  List<StudentMDL> GetStudentDetails(long id=0)
        {
            var todoItem=new List<StudentMDL>();
            if (id!=0) {
                 todoItem = _context.Students.Where(dr=>dr.ID==id).ToList();

            }
            else
            {
                todoItem = _context.Students.ToList();

            }


            return todoItem;
        }

        [HttpGet]
        //[Route("api/Student/GetTodoItem")]
        public List<StudentMDL> GetStudentDetailsByFilter(string SearchBy,string SearchValue)
        {
            var todoItem = new List<StudentMDL>();
            if (SearchBy == "Class")
            {
                todoItem = _context.Students.Where(dr => dr.Class == SearchValue).ToList();

            }
            else
            {
                todoItem = _context.Students.Where(dr => dr.Subject== SearchValue).ToList();
            }


            return todoItem;
        }

        [HttpGet]
        //[Route("api/Student/GetTodoItem")]
        public ActionResult<StudentMDL> DeleteStudent(long id)
        {
            var obj = _context.Students.First(c => c.ID == id);
            _context.Students.Remove(obj);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetStudentDetails), "Deleted Successfully");
        }
    }
}