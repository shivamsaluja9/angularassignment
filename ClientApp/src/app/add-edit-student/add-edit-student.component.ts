import { Component, Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Route, Router } from '@angular/router';

@Component({
    selector: 'app-add-edit-student',
    templateUrl: './add-edit-student.component.html',
    styleUrls: ['./add-edit-student.component.css']
})
/** AddEditStudent component*/
export class AddEditStudentComponent {
    /** AddEditStudent ctor */
  Subjects: any = ['Hindi', 'English', 'Math', 'Science']
  constructor(private http: Http, private route: Router) {

  }
  Submit(form) {
    console.log(form.value);


    this.http.post('https://localhost:44382/api/Student/AddStudentDetails', form.value).subscribe(Response=> {
      alert('Student Added Successfully.');
      this.route.navigate(['']);
    });
  }
}
