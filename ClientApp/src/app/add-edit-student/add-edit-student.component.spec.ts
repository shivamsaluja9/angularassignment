﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { AddEditStudentComponent } from './add-edit-student.component';

let component: AddEditStudentComponent;
let fixture: ComponentFixture<AddEditStudentComponent>;

describe('AddEditStudent component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AddEditStudentComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(AddEditStudentComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});